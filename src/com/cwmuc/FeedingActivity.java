package com.cwmuc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;
//
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
//
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.provider.Settings.Secure;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FeedingActivity extends Activity {
	public String location;
    // Handler for SPL Recorder Message
	private static final int MY_MSG = 1;
	private static String LOCATION = "";
    Thread th;
    TextView tv9;    
    public Handler mhandle = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
            case MY_MSG:
                tv9.setText("" + msg.obj);
                break;
            default:
                super.handleMessage(msg);
                break;
            }
        }
    }; 
    Recorder recorderInstance;
	
    
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feeding);
		
		// Initialise
		final TextView tv02 = (TextView)findViewById(R.id.TextView02);
	    final TextView tv2 = (TextView)findViewById(R.id.textView2);
	    this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);	    
	    
		//Start Recorder
        tv9 = (TextView) findViewById(R.id.textView9);
	    final Recorder recorderInstance;
	    Thread th;  
	    recorderInstance = new Recorder(mhandle);
	    recorderInstance.setRecording(true);
	    th = new Thread(recorderInstance);
	    th.start();
	    
	    
	    // Create Feed
		Intent intent = getIntent();
		String lat = intent.getStringExtra("lat");	
		String lng = intent.getStringExtra("lng");	
		String idp = intent.getStringExtra("idp");	
		String name = intent.getStringExtra("name");	
		String spl = (String) tv02.getText();
		
        try {      	
        	StrictMode.setThreadPolicy(
        			new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().build());
        	
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://www.pachube.com/api.xml");
            httpPost.addHeader("Content-Type", "application/xml");
            httpPost.addHeader("X-PachubeApiKey", "jXET7lLbdVKHfaBcDCVwMMvsCnGSAKwwOTdwdXVjRktYWT0g");
            String xo = "<eeml xmlns='http://www.eeml.org/xsd/0.5.1' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' version='0.5.1' xsi:schemaLocation='http://www.eeml.org/xsd/0.5.1 http://www.eeml.org/xsd/0.5.1/0.5.1.xsd'>  <environment creator='https://cosm.com/users/hdr'>    <title>" + idp + " @ " + name + "</title>    <website></website>    <tag></tag>   <location domain='physical' exposure='indoor' disposition='fixed'>      <name>" + name + "</name>      <lat>" + lat + "</lat>      <lon>" + lng + "</lon>      <ele></ele>    </location>    <data id='0'>      <tag>SPL</tag>      <min_value>0.0</min_value>      <max_value>100</max_value>      <current_value>" + spl + "</current_value>    </data>  </environment></eeml>";        
            StringEntity entity = new StringEntity(xo, "ISO-8859-1");
            entity.setContentType("application/xml");
            httpPost.setEntity(entity);
            HttpResponse httpResponse2 = httpClient.execute(httpPost);           
            String location = httpResponse2.getFirstHeader("Location").getValue().toString();           
            FeedingActivity.LOCATION = location;
            // feed location from input
            tv2.setText(name);
            }
	          catch (Exception e) {
			    tv2.setText(e.toString());
            }	    
	        
	    // stop button
	    Button button1 = (Button)findViewById(R.id.button1);       
	    button1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
	            recorderInstance.setRecording(false);	
	            System.exit(0); 
			} 
		});
	    
	    
	    // On change SPL 
		tv9.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}	
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}	
			@Override
			public void afterTextChanged(Editable s) {
				double valueU = Double.parseDouble(tv02.getText().toString());
				double valueC = Double.parseDouble(tv9.getText().toString());
				double valueCompared = Math.abs(valueU - valueC);
				
				 if (valueCompared > 2.5 ) {
				   tv02.setText(tv9.getText().toString());
				   //Update datastream
				 try {  
		            HttpClient httpClient = new DefaultHttpClient();
		            //HttpPost httpPost = new HttpPost("http://api.cosm.com/v2/feeds/121964/datastreams/0.xml?_method=put");
		            HttpPost httpPost = new HttpPost(FeedingActivity.LOCATION + "/datastreams/0.xml?_method=put");
		            httpPost.addHeader("Content-Type", "application/xml");
		            httpPost.addHeader("X-PachubeApiKey", "jXET7lLbdVKHfaBcDCVwMMvsCnGSAKwwOTdwdXVjRktYWT0g");
		            String xo = "<eeml xmlns='http://www.eeml.org/xsd/0.5.1' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' version='0.5.1' xsi:schemaLocation='http://www.eeml.org/xsd/0.5.1 http://www.eeml.org/xsd/0.5.1/0.5.1.xsd'> <environment><data id='0'> <tag>SPL</tag> <min_value>0.0</min_value><max_value>100</max_value><current_value>" + tv9.getText().toString() + "</current_value> </data> </environment> </eeml>";
		            
		            
		            StringEntity entity = new StringEntity(xo, "UTF-8");
		            entity.setContentType("application/xml");
		            httpPost.setEntity(entity);
		            httpClient.execute(httpPost);    
		            }
		          catch (Exception e) {
				    tv2.setText(e.toString());
	                 }	    
                   //				   
				 }
			}
		});
	    
	    
            
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_feeding, menu);
		return true;
	}
}
