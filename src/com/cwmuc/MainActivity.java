package com.cwmuc;



import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends Activity { 
    
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_main);
		
		//Default for EditText Location
	    TextView ed1 = (EditText)findViewById(R.id.editText1);
		ed1.setText("location");

		// Get phone ID
		TextView tv1 = (TextView)findViewById(R.id.textView1);
		tv1.setText(Secure.getString(getApplicationContext().getContentResolver(),
		           Secure.ANDROID_ID));
		
		// Get Location
		final TextView tv5 = (TextView)findViewById(R.id.textView5);
		final TextView tv6 = (TextView)findViewById(R.id.textView6);
		LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location location) {
				ProgressBar progressBar = (ProgressBar)findViewById(R.id.progressBar1);
				progressBar.setVisibility(View.INVISIBLE); 
	        	double lat = location.getLatitude();
	            double lng = location.getLongitude();
	            tv5.setText(Double.toString(lat));
	            tv6.setText(Double.toString(lng));
			}
			@Override
			public void onProviderDisabled(String provider) {
	            tv5.setText("disabled");
	            tv6.setText("disabled");

			}
			@Override
			public void onProviderEnabled(String provider) {}
			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {}
        };     
    lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,locationListener);				
	
	//Start Feeding
    Button button1 = (Button)findViewById(R.id.button1);       
    button1.setOnClickListener(new View.OnClickListener() {


	@Override
	public void onClick(View v) {
	    Intent myIntent = new Intent(MainActivity.this, FeedingActivity.class);
		final TextView tv5 = (TextView)findViewById(R.id.textView5);
		final TextView tv6 = (TextView)findViewById(R.id.textView6);
		final TextView tv1 = (TextView)findViewById(R.id.textView1);
	    TextView ed1 = (EditText)findViewById(R.id.editText1);
		//final TextView tvSPL = (TextView)findViewById(R.id.textView9);

        String lat = (String) tv5.getText();
        String lng = (String) tv6.getText();
        String idp =  (String) tv1.getText();
        String name =  (String) ed1.getText().toString();
        //String spl =  (String) tvSPL.getText();

  	    myIntent.putExtra("lat", lat);
	    myIntent.putExtra("lng", lng);
	    myIntent.putExtra("idp", idp);
	    myIntent.putExtra("name", name);
	    //myIntent.putExtra("spl", spl);
	  
	    MainActivity.this.startActivity(myIntent); }
	
     });
    
    }

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
